jsql (0.85-0kali1) kali-dev; urgency=medium

  [ Ben Wilson ]
  * Remove template comment and switch spaces to tabs

  [ Sophie Brun ]
  * Update debian/watch to download .jar
  * New upstream version 0.85
  * Revert "Rename install & lintian-overrides" (see 7464)
  * Remove old tranistional package and increases versions in Breaks/ Provides
  * Improve installation
  * Add /usr/bin/jsql-injection link
  * Bump Standards-Version to 4.6.0
  * Remove useless lintian-overrides

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 30 Nov 2021 11:08:24 +0100

jsql (0.82-0kali3) kali-dev; urgency=medium

  * Rename install & lintian-overrides

 -- Ben Wilson <g0tmi1k@kali.org>  Thu, 18 Nov 2021 09:54:58 +0000

jsql (0.82-0kali2) kali-dev; urgency=medium

  *  Use jar-wrappers instead of jarwrapper

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 20 Feb 2020 09:32:23 +0100

jsql (0.82-0kali1) kali-dev; urgency=medium

  [ Sophie Brun ]
  * New upstream version 0.82
  * jsql is now compatible with newer Java: use javawrapper
  * Use debhelper-compat 12
  * Bump Standards-Version to 4.5.0

  [ Raphaël Hertzog ]
  * Update Vcs-* fields for the move to gitlab.com
  * Add GitLab's CI configuration file
  * Configure git-buildpackage for Kali
  * Update URL in GitLab's CI configuration file

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 18 Feb 2020 16:23:35 +0100

jsql (0.81-0kali2) kali-dev; urgency=medium

  * Use java 9 in helper-script (fix 5122). jsql is not ready for java 11
    (https://github.com/ron190/jsql-injection/issues/90372)
  * Update Maintainer and Uploaders fields
  * Bump Standards-Version to 4.2.1

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 11 Dec 2018 10:35:51 +0100

jsql (0.81-0kali1) kali-dev; urgency=medium

  * Import new upstream release
  * Add option "--add-modules java.xml.bind" in helper-script (see bug 4656)

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 29 Mar 2018 10:35:22 +0200

jsql (0.79-0kali2) kali-dev; urgency=medium

  * Add a transitional package jsql

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 12 May 2017 10:00:35 +0200

jsql (0.79-0kali1) kali-dev; urgency=medium

  * Import new upstream release
  * Change binary package name: jsql to jsql-injection (see 3955)

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 05 May 2017 14:40:31 +0200

jsql (0.74-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 19 May 2016 11:12:37 +0200

jsql (0.5-1kali0) kali; urgency=low

  * Upgrade to 0.5

 -- Emanuele Acri <crossbower@kali.org>  Tue, 15 Oct 2013 12:40:39 +0200

jsql (0.4-1kali0) kali; urgency=low

  * Initial release (Closes: #0000120)

 -- Devon Kearns <dookie@kali.org>  Thu, 13 Jun 2013 08:09:56 -0600
